/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronfachada;

import patronfachada.subsistemas.Fachada;

/**
 *
 * @author ADMIN
 */
public class Cliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Fachada fachada = new Fachada();
        // Se hace uso de las operaciones de alto nivel
        fachada.compra();
        fachada.compra();
        fachada.compra();
    }
    
}
