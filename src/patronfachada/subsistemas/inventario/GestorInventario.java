/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronfachada.subsistemas.inventario;

import java.util.Scanner;

/**
 *
 * @author ADMIN
 */
public class GestorInventario {
    
    private int stock;

    public GestorInventario() {
        this.stock = 2;
    }
    
    public boolean retirarStock(){
        if(stock > 0){
            System.out.println("Producto listo para envio");
            stock--;
            return true;
        }else{
            System.out.println("Producto no disponible, no hay existencias, entrega reprogramada!");
            return false;
        }
    }
    
    
}
