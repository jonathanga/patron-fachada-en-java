/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronfachada.subsistemas.compras;

import java.util.Scanner;

/**
 *
 * @author ADMIN
 */
public class GestorCompra {
    private Scanner in = new Scanner(System.in);
    
    public boolean comprar(){
        int numero;
        System.out.println("Ingrese el numero de tarjeta para realizar el pago");
        numero = in.nextInt();
        if(numero == 4567){
            System.out.println("Procesando la compra");
            System.out.println("--------------------");
            System.out.println("Pago aceptado");
            return true;
        }else{
            System.out.println("Pago rechazado");
            return false;
        }
    }
}
